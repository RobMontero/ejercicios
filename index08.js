"use strict";

/* Escribe una función que devuelva un array de usuarios. De cada usuario guardaremos el username, el nombre y apellido, el sexo, el país, el email y un enlace a su foto de perfil.

El número de usuarios a obtener se debe indicar con un parámetro de dicha función.

Sírvete de la API: https://randomuser.me/ */


async function dataget() {                    
    const response = await fetch('https://randomuser.me/api');           
    const data = await response.json();          
    return data
}

async function users() {

    let datos = [];
    const n = 10;

    for(let i = 0; i < n; i++){

        let data = await dataget();
        datos.push('USERNAME: ' + data.results[0].login.username + ', NOMBRE: ' + data.results[0].name.first + ', APELLIDO: ' + data.results[0].name.last + ', SEXO: ' + data.results[0].gender + ', PAIS: ' + data.results[0].location.country + ', EMAIL: ' + data.results[0].email + ', FOTO DE PERFIL: ' + data.results[0].picture.large);

    }
    
    for(let i = 0; i < n; i++){
        console.log(datos[i]);
    }
}

users();
