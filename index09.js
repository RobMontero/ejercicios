'use strict'

/* Ejercicio 2

Crea un programa que imprima cada 5 segundos el tiempo desde la ejecución del mismo. Formatea el tiempo para que se muestren los segundos, los minutos, las horas y los días desde la ejecución. */


"use strict";

let segundos = 0;  
let minutos  = 0;  
let horas    = 0;  
let dias     = 0;  

const intervalo = setInterval(() => reloj(), 1000);  

function reloj() {
    segundos++;
    if (segundos === 60) {  
        minutos++;
        segundos = 0; 
    }
    if (minutos === 60) {  
        horas++; 
        minutos = 0; 
    }
    if (horas === 24) {  
        dias++; 
        horas = 0; 
    }

    if (segundos % 5 === 0) {
        const tiempo = `Tiempo transcurrido ${dias} dias, ${horas} horas, ${minutos} minutos y ${segundos} segundos`;
        console.log(tiempo);
    }
}
