'use strict'

/* Ejercicio 3

Crea un programa que reciba un número en decimal o binario y devuelva la conversión:

-   Si le pasamos un nº en decimal debe retornar la conversión a binario.

-   Si le pasamos un nº en binario deberá devolver su equivalente decimal.

Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).

No se permite utilizar el método parseInt().*/


"use-strict";

function checkNumberBase(nmbr) {
    const nmbrString = nmbr.toString();
    for (let i = 0; i < nmbrString.length; i++) {       
        if (nmbrString[i] !== "1" && nmbrString[i] !== "0") {
            return 10;
        }
    }
    return 2;       }

function numberConverter(nmbr, bs) {
    let nmbrArray = [];
    let finalNumber = 0;  
    let base = checkNumberBase(nmbr); 

    if (bs === 10) { 
        base = 10;
    }

    switch (base) {
        case 2: 
        nmbrArray = String(nmbr)
                .split("")
                .reverse()
                .map((nmbr) => {
                    return Number(nmbr);
                });                                

            for (let i = 0; i < nmbrArray.length; i++) {  
                finalNumber += nmbrArray[i] * Math.pow(2, i); 
            }
            break;

        case 10: 
            while (nmbr >= 1) {   
                if (nmbr % 2 === 0) {
                    nmbrArray.push("0");  
                } else {
                    nmbrArray.push("1");  
                }
                nmbr = Math.trunc(nmbr / 2);  
            }
            

            nmbrArray.reverse();

            for (let i = 0; i < nmbrArray.length; i++) { 
                if (nmbrArray[i] === "1") {
                    finalNumber += Math.pow(10, nmbrArray.length - i - 1);  
                }
            }
            break;
    }
    return finalNumber;
}

console.log(numberConverter(3));
console.log(numberConverter(11));
