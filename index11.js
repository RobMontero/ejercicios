/* Ejercicio 4

Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo.

No se permite hacer uso de Set ni Array.from().

```javascript
// Ejemplo
const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
]; */


'use-strict';


const names = [  
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];

function duplicados(lista) {
    const resultado = lista.filter((item, pos) => {
        return lista.indexOf(item) == pos 
    })
    return resultado
}

console.log(duplicados(names));
