"use-strict";

const api = "https://rickandmortyapi.com/api/";  

async function getData(url) {  
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

async function getEpisodesData(url) { 
    const data = await getData(url);
    const episodesData = await getData(data.episodes);
    const episodes = episodesData.results;
    return episodes;
}

async function getCharsData(url, mes) { 
    let charactersSet = new Set();
    let characterArray = [];
    const episodes = await getEpisodesData(url);

    for (const episode of episodes) {
    const monthString = episode.air_date.substring(0, 3);
    const yearString = episode.air_date.slice(-4);

    if (monthString === mes) {
    for (const character of episode.characters) {
                charactersSet.add(character);
    }
    }


    }
     
    for (const character of charactersSet) {
        const charData = await getData(character);
        const charObject = {
            name: charData.name,
            gender: charData.gender,
            species: charData.species,
            status: charData.status,
            image: charData.image,
        };
        characterArray.push(charObject);
        console.log(charData.name); 
    }

    placeCharacters(characterArray, mes)
}

function placeCharacters(characters, mes) {

    const p = document.querySelector('p'); 
    p.innerText = `Episodios estrenados en ${mes}: ` 
    const list = document.querySelector('ul'); 
    for (const character of characters) { 
        const li = document.createElement('li'); 
        li.innerHTML = character.name; 
        list.append(li);
    }
}

getCharsData(api, "Jan");
